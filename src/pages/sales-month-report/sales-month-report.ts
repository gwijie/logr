import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { SalesMonthResPage } from '../sales-month-res/sales-month-res';

//@IonicPage()
@Component({
  selector: 'page-sales-month-report',
  templateUrl: 'sales-month-report.html',
})
export class SalesMonthReportPage {
  date: string;
  salesMonthReportData: any;

  constructor(public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad SalesReportPage');
  }

  onSelectChange(selectedValue: any)
  {
    this.date = selectedValue.year+'-'+selectedValue.month+'-'+selectedValue.day;
    console.log('Selected', this.date);
  }

  getSalesMonthReport(datei){
    let data = {
      date: datei,
    }

   this.navCtrl.push(SalesMonthResPage,data);
  }

}
