import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';

//@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  feed: any;
  limit: any;
  limitDate: any;
  year: any;
  month: any;
  day: any;
  da: any;
  det: any;

  private getDataUrl = 'http://app.kirimiridairy.co.ke/api/getNewMembers';
  private getCustomersUrl = 'http://app.kirimiridairy.co.ke/api/getCustomers';
  private getPricelistUrl = 'http://app.kirimiridairy.co.ke/api/getPricelist';
  private getStationsUrl = 'http://app.kirimiridairy.co.ke/api/getStations';
  data: any = {};
  user_id: any;
  
  constructor(private fire: AngularFireAuth,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,public http: Http,public DB: DatabaseProvider) {
  this.user_id = this.fire.auth.currentUser.email;
  }


  getDits(){
    return this.http.get(this.getDataUrl).map((res) => res.json())
  }
  
  getCustomers(){
    return this.http.get(this.getCustomersUrl).map((res) => res.json())
  }

  pullMembers(){
    let loader = this.loadingCtrl.create({
      content: "Syncing...",
    });
    loader.present().then(() =>{
      this.DB.pullDB()
      .on('complete', function (info) {
        loader.dismiss();
      });
    });
    
  }

  getPricelist(){
    return this.http.get(this.getPricelistUrl).map((res) => res.json())
  }
 
  getStations(){
    return this.http.get(this.getStationsUrl).map((res) => res.json())
  }
  /*
  async getMembaz()
  {
     let loader = this.loadingCtrl.create(
       {
        content: "Please wait...",
        });

      loader.present();
    try
    {
        this.getDits().subscribe((data) => 
        {
          var i = 0;
          for(i = 0; i < (data as any).length; i++)
          {
            
            var todo = 
            {
              //_id: new Date().toISOString(),
              fullNames: data[i].fullNames,
              membershipNo: data[i].membershipNo,
              route_name: data[i].route_name,
              mobileNo: data[i].mobileNo,
              nationalID: data[i].nationalID
            };
            
            this.DB.syncingMembers(todo);
          
          }
        });
   }
   finally
   {
      loader.dismiss().then(() => {
        let alert = this.alertCtrl.create({
          title: 'Successul Sync',
          subTitle: 'Members have been updated',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel'
            }
          ]
        });
  
        alert.present();
      });
    
    }


  }
*/
  /*
  async fetchCustomers()
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });

    loader.present();
    try
    {
      this.getCustomers().subscribe((data) => {
        this.data = data;
        var i = 0;
        
        for(i = 0; i < this.data.length; i++){
          var todo = {
            //_id: new Date().toISOString(),
            customer_id: data[i].customer_id,
            customer_name: data[i].customer_name,
          };
          this.DB.syncingCustomers(todo);
        }
      });
    }
    finally{
            loader.dismiss().then(() => {
              let alert = this.alertCtrl.create({
                title: 'Successul Sync',
                subTitle: 'Customers have been updated',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel'
                  }
                ]
              });
              alert.present();
            });
          }
  }
*/
  /*
  pullCustomers(){
    //this.getCustomers();
    this.fetchCustomers();
  }
*/
   /*
  async fetchPricelist()
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    
    loader.present();
    try{
            this.getPricelist().subscribe((data) => {
              this.data = data;
              var i = 0;
              
              for(i = 0; i < this.data.length; i++){
                var todo = {
                  //_id: new Date().toISOString(),
                  product_id: data[i].product_id,
                  product_name: data[i].product_name,
                  product_capacity: data[i].product_capacity,
                  product_price: data[i].product_price,
                };
                this.DB.syncingPricelist(todo);
              }
            })
        }
    finally{
            loader.dismiss().then(() => {
              let alert = this.alertCtrl.create({
                title: 'Successul Sync',
                subTitle: 'Products have been updated',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel'
                  }
                ]
              });
              alert.present();
            });
          }
  }
*/
  /*
  async fetchStations()
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
      try{
            this.getStations().subscribe((data) => {
              this.data = data;
              var i = 0;
              
              for(i = 0; i < this.data.length; i++)
              {
                var todo = {
                  //_id: new Date().toISOString(),
                  route_id: data[i].route_id,
                  route_name: data[i].route_name,
                };
                this.DB.syncingStation(todo);
              }
              });
          }
    finally{
            loader.dismiss().then(() => {
              let alert = this.alertCtrl.create({
                title: 'Successul Sync',
                subTitle: 'Station have been updated',
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel'
                  }
                ]
              });
              alert.present();
            });
          }
  }
  
  pullPricelist(){
    //this.getPricelist();
    this.fetchPricelist();
  }

  pullStations(){
    //this.getStations();
    this.fetchStations();
  }

  saveDocs(docs){

    return new Promise(resolve => {
      this.DB.syncingMembers(docs).then((res) => {
        console.log('call res '+res);
      }).catch((e) => {
        console.log(e);
      });

      resolve(true);
      console.log('docs are'+docs);
    });
  }
  */
}
