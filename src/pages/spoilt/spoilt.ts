import { Component } from '@angular/core';
import { NavController, NavParams , AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import { SpoiltResPage } from '../spoilt-res/spoilt-res';

//@IonicPage()
@Component({
  selector: 'page-spoilt',
  templateUrl: 'spoilt.html',
})
export class SpoiltPage {

  spoiltForm: FormGroup;
  requiredErrMsg: string;
  volumePatternErrMsg: string;
  maxErrMsg: string;
  user_id: any;
  spoiltVolume: any;
  spoiltStation: any;

  private stations;

  constructor(private fire: AngularFireAuth,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public DB: DatabaseProvider) {
    this.user_id = this.fire.auth.currentUser.email;
    this.initializedSpoiltErrors();
    
        this.spoiltForm = formBuilder.group({
          spoiltVolume: ['', Validators.compose([Validators.maxLength(7),Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
          spoiltStation: ['', Validators.compose([Validators.required])],
      });
  }

  ionViewDidLoad() {
    this.loadStations();
  }

  initializedSpoiltErrors(){
    this.requiredErrMsg = 'This is required';
    this.volumePatternErrMsg = 'Please enter a valid weight eg. 5.80';
    this.maxErrMsg = 'Field must have not more than 6 digits'; 
  }

  loadStations()
  {
    this.DB.fetchStations().then((result) =>
    {
      this.stations = [];
      
          let row = (result as any).rows;    
          for(let i = 0; i < row.length; i ++ )
          {    
              this.stations.push(row[i].doc);   
          }

    });
  }

  saveSpoilt(){
    {
      var ddate = new Date();
      var tiime = ddate.toTimeString().substr(0,8);
      var daate = ddate.toISOString().substr(0,10);
      var created_at = daate+' '+tiime;
      let spoiltObject = this.spoiltForm.controls['spoiltStation'].value;
  
      var spoiltData = {
        _id: new Date().toJSON() + Math.random(),
        weight: this.spoiltVolume,
        station_id: spoiltObject.route_id,
        station_name: spoiltObject.route_name,
        time: tiime,
        date: new Date().toString().substr(0,25),
        created_at: created_at,
        user_id: this.user_id,
        db: 'spoilt',
      }
  
      this.DB.createSpoilt(spoiltData).then((result) =>
    {
      if(result)
      {
        
          let alert = this.alertCtrl.create({
            title: 'Successfully Saved',
            subTitle: 'Spoilt milk has been recorded.',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  this.spoiltForm.reset();
                }
              }
            ]
          });
          alert.present();
      }
      else
      {
        let alert = this.alertCtrl.create({
          title: 'Oops',
          subTitle: 'Seem a error has occured',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
              handler: () => {
                this.spoiltForm.reset();
              }
            }
          ]
        });
        alert.present();
      }
    });
  
    }
  }

}
