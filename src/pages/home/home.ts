import { Component } from '@angular/core';
import { NavController, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LookupPage } from '../lookup/lookup';
import { PricelistPage } from '../pricelist/pricelist';
import { ProcurementReportPage } from '../procurement-report/procurement-report';
import { StatementReportPage } from '../statement-report/statement-report';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public hasdeta     : boolean = false;
  public data        : any;

  user_id: string;
  spoiltData: any;
  receivedData: any;
  receivedMonthData: any;
  procureData: any;
  feedsData: any;
  salesData: any;
  procureMonthData: any;
  salesMonthData: any;
  year: any;
  month: any;

  constructor(public alertCtrl: AlertController,public loadingCtrl: LoadingController, private fire: AngularFireAuth,public menuCtrl: MenuController ,public navCtrl: NavController,public DB: DatabaseProvider) {
    this.user_id = this.fire.auth.currentUser.email;
    this.sync();
    this.DB.change()
    .on('change', (change) => {
      this.refresher();
    });

  }

  ionViewWillEnter()
  {
    //this.synk(); 
    this.refresher();

    this.menuCtrl.enable(true);
    if(this.user_id === 'stephen@conradgray.com' || this.user_id === 'admin@kirimiridairy.co.ke')
    {
      this.menuCtrl.enable(false, 'notAdmin');
      this.menuCtrl.enable(true, 'Admin');
    }
    else{
      this.menuCtrl.enable(true, 'notAdmin');
      this.menuCtrl.enable(false, 'Admin');
    }
  }

  refresher(){
    this.displayProcure();
    this.displayReceived();
    this.displaySales();
    this.displayMonthlyProcure();
    this.displayMonthlyReceived();
    this.displayMonthSales();
  }

  synk(){
    this.DB.syncEverything();
  }

  sync(){
    var msg = "Syncing...";

    let loader = this.loadingCtrl.create({
      content: msg,
    });
    
    loader.present().then(() =>{
      this.DB.pullDB()
      .on('complete', function (info) {
        loader.dismiss();
      })
      .on('error', function (err) {
        loader.dismiss().then(() => {
          let alert = this.alertCtrl.create({
            title: '',
            subTitle: 'Sync has been interupted',
            buttons: ['OK']
          });
          alert.present();
        });
      });
    });

    
  }

  displaySpoilt()
  {
     
  }

  displayReceived()
  {
    this.DB.retrieveReceived()
    .then((dataReceived)=>
    {

      let sum = 0;
      for (var i = 0; i < (dataReceived as any).length; i++) {
          sum+= +dataReceived[i].value.document.weight;
      }

      this.receivedData = sum;
      
    });
  }

  displaySales()
  {
     this.DB.retrieveSales()
     .then((dataSales)=>
     {
      let sum = 0;
      for (var i = 0; i < (dataSales as any).length; i++) {
          sum+= +dataSales[i].value.document.weight;
      }
       this.salesData = sum;
     });
  }

  displayMonthSales()
  {
     this.DB.retrieveMonthlySales().then((dataMonthSales)=>
     {
      let sum = 0;
      for (var i = 0; i < (dataMonthSales as any).length; i++) {
          sum+= +dataMonthSales[i].value.document.weight;
      }
       this.salesMonthData = sum;
     });
  }

  displayProcure()
  {
     this.DB.retrieveProcure()
     .then((data)=>
     {

       let sum = 0;
       for (var i = 0; i < (data as any).length; i++) {
           sum+= +data[i].value.weight;
       }
       this.procureData = sum;
     });
  }

  displayMonthlyProcure()
  {
     this.DB.retrieveMonthlyProcure()
     .then((dataProcure)=>
     {

       let sum = 0;
       for (var i = 0; i < (dataProcure as any).length; i++) {
           sum+= +dataProcure[i].value.weight;
       }
       this.procureMonthData = sum;
     });
  }

  displayMonthlyReceived(){
    this.DB.retrieveMonthlyReceived()
    .then((dataMonthReceived)=>
    {

      let sum = 0;
      for (var i = 0; i < (dataMonthReceived as any).length; i++) {
          sum+= +dataMonthReceived[i].value.document.weight;
      }
      this.receivedMonthData = sum;
    });
  }

  lookup()
  {
    this.navCtrl.push(LookupPage);
  }

  priceList()
  {
    this.navCtrl.push(PricelistPage);
  }

  statement()
  {
    this.navCtrl.push(StatementReportPage);
  }

  procurementRecords()
  {
    this.navCtrl.push(ProcurementReportPage);
  }

}
