import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

//@IonicPage()
@Component({
  selector: 'page-sale',
  templateUrl: 'sale.html',
})
export class SalePage {

  saleForm: FormGroup;
  user_id: any;
 
  private saleVolume;
  private salePrice;
  //private saleCustomer;
  private saleTx;
  private customers;

  requiredErrMsg: string;
  volumePatternErrMsg: string;
  pricePatternErrMsg: string;
  txPatternErrMsg: string;
  customer_name: any;

  constructor(private fire: AngularFireAuth,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public DB: DatabaseProvider) {
    this.user_id = this.fire.auth.currentUser.email;
    this.initializedSaleErrors();
    
        this.saleForm = formBuilder.group({
          saleVolume: ['', Validators.compose([Validators.maxLength(15),Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
          salePrice: ['', Validators.compose([Validators.maxLength(15),Validators.required,Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
          saleCustomer: ['', Validators.compose([Validators.required])],
          saleTx: ['', Validators.compose([Validators.maxLength(15),Validators.required,Validators.pattern('^[A-Z]{3}[0-9]{1}[A-Z]([0-9]|[A-Z]){5}$')])]
      });
  }

  ionViewDidLoad() {
    this.loadCustomers();
  }

  initializedSaleErrors(){
    this.requiredErrMsg = 'This is required';
    this.volumePatternErrMsg = 'Please enter a valid weight eg. 5.80';
    this.pricePatternErrMsg = 'Please enter a valid price eg. 40.50';
    this.txPatternErrMsg = 'Please enter a valid transaction code';
  }

  loadCustomers()
  {
    this.DB.fetchCustomers().then((result) =>
    {
      this.customers = [];
      
          let row = (result as any).rows;
          for(let i = 0; i < row.length; i ++ )
          {    
              this.customers.push(row[i].doc);   
          }
    });

  }


  saveSale()
  {
    var ddate = new Date();
    var tiime = ddate.toTimeString().substr(0,8);
    var daate = ddate.toISOString().substr(0,10);
    var created_at = daate+' '+tiime;
    var amount = this.salePrice * this.saleVolume;
    let customerObject = this.saleForm.controls['saleCustomer'].value;


    var salez = {
      _id: new Date().toJSON() + Math.random(),
      weight: this.saleVolume,
      customer_id: customerObject.customer_id,
      customer_name: customerObject.customer_name,
      amount: amount,
      price: this.salePrice,
      date: new Date().toString().substr(0,25),
      created_at: created_at,
      tx_code: this.saleTx,
      user_id: this.user_id,
      db: 'sale',
    }

    this.DB.createSale(salez).then((result) =>
  {
    if(result)
    {
      
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Sale has been recorded.',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
              handler: () => {
                this.saleForm.reset();
              }
            }
          ]
        });
        alert.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Oops',
        subTitle: 'Seem a error has occured',
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: () => {
              this.saleForm.reset();
            }
          }
        ]
      });
      alert.present();
    }
  });

  }

}
