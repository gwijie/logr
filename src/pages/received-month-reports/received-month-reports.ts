import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { ReceivedMonthResPage } from '../received-month-res/received-month-res';


//@IonicPage()
@Component({
  selector: 'page-received-month-reports',
  templateUrl: 'received-month-reports.html',
})
export class ReceivedMonthReportsPage {

  date: string;
  receivedMonthReportData: any;

  constructor(public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad SalesReportPage');
  }

  onSelectChange(selectedValue: any)
  {
    this.date = selectedValue.year+'-'+selectedValue.month+'-'+selectedValue.day;
    console.log('Selected', this.date);
  }

  getReceivedMonthReport(datei){
    let data = {
      date: datei,
    }

   this.navCtrl.push(ReceivedMonthResPage,data);
  }

}
