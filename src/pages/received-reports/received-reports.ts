import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { ReceivedResPage } from '../received-res/received-res';


//@IonicPage()
@Component({
  selector: 'page-received-reports',
  templateUrl: 'received-reports.html',
})
export class ReceivedReportsPage {
  date: string;
  salesReportData: any;

  constructor(public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  onSelectChange(selectedValue: any)
  {
    this.date = selectedValue.year+'-'+selectedValue.month+'-'+selectedValue.day;
    console.log('Selected', this.date);
  }

  getReceivedReport(datei){
    let data = {
      date: datei,
    }

   this.navCtrl.push(ReceivedResPage,data);
  }
}
