import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular';

//@IonicPage()
@Component({
  selector: 'page-sales-month-res',
  templateUrl: 'sales-month-res.html',
})
export class SalesMonthResPage {

  salesMonthReportData: any;
  dat: any;
  monthSaleSum: any;
  user_id: any;

  constructor(public loadingCtrl: LoadingController,private fire: AngularFireAuth,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
    
  }

  ionViewDidLoad() {
    this.dat = this.navParams.get('date');
    this.getSalesMonthReport(this.dat);
  }

  getSalesMonthReport(datei){

    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.DB.retrieveSalesMonthReport(datei)
    .then((dataSales)=>
    {
      console.log(datei);
      let existingData = Object.keys(dataSales).length;

      if(existingData !== 0)
      {
              this.salesMonthReportData   = [];
              let sum = 0;
              for(let i = 0; i < (dataSales as any).length; i ++ )
               {    
                   this.salesMonthReportData.push(dataSales[i].value.document);   
                   sum += +dataSales[i].value.document.weight;
               }

               this.monthSaleSum = sum;
               
               
      }
      else
      {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
      }

    }).then(() => {
      loader.dismiss();
    });
 }

 deleteEntry(doc)
 {
   this.DB.deleteSalesEntryDoc(doc).then((info) => {

     let alert = this.alertCtrl.create({
       title: 'Delete!',
       subTitle: 'Entry has been deleted',
       buttons: [{
         text: 'OK',
         role: 'cancel',
         handler: () => {
           
           alert.dismiss();
       }
     }]
     });

     alert.present();
   });
 }
 
 showInfo(data) {
  let alert = this.alertCtrl.create({
    title: '',
    subTitle: '<b>Customer: </b>'+data.customer_name+'<br><b>Volume</b>: '+data.weight.toString().split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," )+' Kg <br><b>Price</b>: Kes. '+data.price+'<br> <b>Amount Due: </b>Kes. '+Math.round(data.amount).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'<br> <b>Tx Code: </b> '+((data.tx_code === 'AAA1A1111A') ? 'CREDIT' : data.tx_code) +'<br> <b>Date</b>: '+data.date,
    buttons: ['OK']
  });
  alert.present();
}

}
