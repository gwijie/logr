import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { SpoiltResPage } from '../spoilt-res/spoilt-res';


//@IonicPage()
@Component({
  selector: 'page-spoilt-report',
  templateUrl: 'spoilt-report.html',
})
export class SpoiltReportPage {

  date: any;

  constructor(public DB: DatabaseProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpoiltReportPage');
  }

  getSpoiltReport(){
    let data = {
      date: this.date
    }
   this.navCtrl.push(SpoiltResPage,data);
  }

}
