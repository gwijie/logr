import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

//@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  email: any;
  constructor(private fire: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
  this.email = this.fire.auth.currentUser.email;
  }

}
