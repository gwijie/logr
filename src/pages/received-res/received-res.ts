import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular';

//@IonicPage()
@Component({
  selector: 'page-received-res',
  templateUrl: 'received-res.html',
})
export class ReceivedResPage {

  receivedReportData: any;
  dat: any;
  receivedSum: any;
  user_id: any;

  constructor(public loadingCtrl: LoadingController,private fire: AngularFireAuth,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
  }

  ionViewDidLoad() {
    this.dat = this.navParams.get('date');
    this.getReceivedReport(this.dat);
  }

  getReceivedReport(datei){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

     this.DB.retrieveReceivedReport(datei)
     .then((dataSales)=>
     {

       let existingData = Object.keys(dataSales).length;
       let sum = 0;
 
       if(existingData !== 0)
       {
               this.receivedReportData   = [];
               for(let i = 0; i < (dataSales as any).length; i ++ )
                {    
                    this.receivedReportData.push(dataSales[i].value.document);
                    sum+= +dataSales[i].value.document.weight;
                }
                this.receivedSum = sum;
       }
       else
       {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
       }
     }).then(() => {
      loader.dismiss();
     });
  }

  deleteEntry(doc)
  {

    this.DB.deleteReceivedEntryDoc(doc).then((info) => {

      let alert = this.alertCtrl.create({
        title: 'Delete!',
        subTitle: 'Entry has been deleted',
        buttons: [{
          text: 'OK',
          role: 'cancel',
          handler: () => {
            
            alert.dismiss();
        }
      }]
      });
      alert.present();
    });
  }

}
