import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { StatementResPage } from '../statement-res/statement-res';
import { Datemember } from '../../model/datemember';

//@IonicPage()
@Component({
  selector: 'page-statement-report',
  templateUrl: 'statement-report.html',
})
export class StatementReportPage {

  statementForm: FormGroup;
  requiredErrMsg: string;
  volumePatternErrMsg: string;
  maxErrMsg: string;
  minErrMsg: string;
  no: any;
  info = {} as Datemember;

  statementData: any;
  dat: any;
  combo: any;
  parse: any;
  

  constructor(public formBuilder: FormBuilder,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
        this.initializedReceiveErrors();

        this.statementForm = formBuilder.group({
          memberNo: ['', Validators.compose([Validators.minLength(1),Validators.maxLength(4),Validators.required, Validators.pattern('^[0-9]*[1-9]+$|^[1-9]+[0-9]*$')])],
          date: []
      });
  }

  ionViewDidLoad() {
  }

  initializedReceiveErrors(){
    this.requiredErrMsg = 'This is required';
    this.volumePatternErrMsg = 'Please enter a valid number eg. 008';
    this.maxErrMsg = 'Field must have not more than 4 digits'; 
    this.minErrMsg = 'Field must have atleast 1 digit'; 
  }

  valueChange(selectedValue: any){
    this.no = selectedValue;
  }

  getStatement(){
    let data = {
      date: this.info.date,
      no: this.no
    }
    console.log(data);
   this.navCtrl.push(StatementResPage,data);
  }
  
/*
   getStatement(){
    this.statementData   = [];
    this.parse = this.info.date+'-'+this.no;
    
    this.DB.retrieveStatement(this.parse)
    .then((dataStatement)=>
    {  
      let existingData = Object.keys(dataStatement).length;
      if(existingData !== 0)
      {
              let sum = 0;
              for(let i = 0; i < (dataStatement as any).length; i ++ )
               {    
                   this.statementData.push(dataStatement[i].value.document);   
                   sum += +dataStatement[i].value.document.weight;
               }

               this.totalSum = sum;
      }
      else
      {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'We seem not to have data on the farmer',
          buttons: ['Dismiss']
        });
        alert.present();
      }
      
    });
  
}
*/


}
