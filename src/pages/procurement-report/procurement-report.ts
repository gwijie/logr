import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { ProcurementResPage } from '../procurement-res/procurement-res';


//@IonicPage()
@Component({
  selector: 'page-procurement-report',
  templateUrl: 'procurement-report.html',
})
export class ProcurementReportPage {

  date: string;
  procurementReportData: any;

  constructor(public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad SalesReportPage');
  }

  onSelectChange(selectedValue: any)
  {
    this.date = selectedValue.year+'-'+selectedValue.month+'-'+selectedValue.day;
    console.log('Selected', this.date);
  }

  getProcurementReport(datei){
    let data = {
      date: datei,
    }
   this.navCtrl.push(ProcurementResPage,data);
  }

}
