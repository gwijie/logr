import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { Receive } from '../../model/receive';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';

//@IonicPage()
@Component({
  selector: 'page-received',
  templateUrl: 'received.html',
})
export class ReceivedPage {

  receiveForm: FormGroup;
  receive = {} as Receive;
  requiredErrMsg: string;
  volumePatternErrMsg: string;
  maxErrMsg: string;
  user_id: any;

  private stations;
  receiveVolume: any;
  receiveStation: any;
  receiveTime: any;

  constructor(private fire: AngularFireAuth,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public DB: DatabaseProvider) {
    this.user_id = this.fire.auth.currentUser.email;
    this.initializedReceiveErrors();
    
        this.receiveForm = formBuilder.group({
          receiveVolume: ['', Validators.compose([Validators.maxLength(7),Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
          receiveStation: ['', Validators.compose([Validators.required])],
          receiveTime: ['', Validators.compose([Validators.required])]
      });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ReceivedPage');
    this.loadStations();
  }

  loadStations()
  {
    this.DB.fetchStations().then((result) =>
    {
      this.stations = [];
      
          let row = (result as any).rows;
                
          for(let i = 0; i < row.length; i ++ )
          {    
              this.stations.push(row[i].doc);   
          }

    });

  }

  initializedReceiveErrors(){
    this.requiredErrMsg = 'This is required';
    this.volumePatternErrMsg = 'Please enter a valid weight eg. 5.80';
    this.maxErrMsg = 'Field must have not more than 6 digits'; 
  }

  saveReceived()
  {
    var ddate = new Date();
    var tiime = ddate.toTimeString().substr(0,8);
    var daate = ddate.toISOString().substr(0,10);
    var created_at = daate+' '+tiime;
    let receivedObject = this.receiveForm.controls['receiveStation'].value;

    var resived = {
      _id: new Date().toJSON() + Math.random(),
      weight: this.receiveVolume,
      station_id: receivedObject.route_id,
      station_name: receivedObject.route_name,
      time: this.receiveTime,
      tyme: tiime,
      date: new Date().toString().substr(0,25),
      created_at: created_at,
      user_id: this.user_id,
      db: 'received',
    }
    
    this.DB.createReceived(resived).then((result) =>
  {
    if(result)
    {
      
        let alert = this.alertCtrl.create({
          title: 'Successully Saved',
          subTitle: 'Receipt has been recorded.',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
              handler: () => {
                this.receiveForm.reset();
              }
            }
          ]
        });
        alert.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Oops',
        subTitle: 'Seem a error has occured',
        buttons: [
          {
            text: 'Ok',
            role: 'cancel'
          }
        ]
      });
      alert.present();
    }
  });

  }

}
