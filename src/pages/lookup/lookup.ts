import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { Member } from '../../model/member';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BuyMilkPage } from '../buy-milk/buy-milk';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

//@IonicPage()
@Component({
  selector: 'page-lookup',
  templateUrl: 'lookup.html',
})
export class LookupPage {

  lookupForm: FormGroup;
  member = {} as Member;
  private membaz;
  
  requiredErrMsg: string;
  maxErrMsg: string;
  minErrMsg: string;
  numberErrMsg: string;

  constructor(public loadingCtrl: LoadingController ,public navCtrl: NavController,public formBuilder: FormBuilder, public navParams: NavParams, public DB: DatabaseProvider) {
    this.initializeErrorz();
    
        this.lookupForm = formBuilder.group({
          membershipNo: ['', Validators.compose([Validators.minLength(3),Validators.maxLength(4),Validators.required, Validators.pattern('^[0-9]*[0-9]+$|^[0-9]+[0-9]*$')])]
      });
  }

  initializeErrorz(){
    this.requiredErrMsg = 'This is required';
    this.minErrMsg = ' No. must be at least 3 digits long';
    this.maxErrMsg = ' No. must not be longer than 4 digits long';
    this.numberErrMsg = 'Field must be at be a number';
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LookupPage');
  }

  lookup(x)
  {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.DB.lookUp(x).then((data)=>
    {
      console.log(data);

      this.membaz = [];
      
          let row = (data as any).rows;
                
          for(let i = 0; i < row.length; i ++ )
          {    
              this.membaz.push(row[i].doc);   
          }

         // console.log(row);
       //this.membaz.push(data);
       
    }).then(()=>{
      loader.dismiss();
    });
  }

  buyMilk(member: Member)
  {
    let data = {
      no: member.membershipNo,
      name: member.fullNames,
      station: member.route_name,
    }

    this.navCtrl.push(BuyMilkPage, data);
    //console.log(data);
  }

  issueFeed()
  {
    console.log('Go to buy issue feed page');
  }

}
