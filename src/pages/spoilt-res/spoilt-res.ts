import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular';


//@IonicPage()
@Component({
  selector: 'page-spoilt-res',
  templateUrl: 'spoilt-res.html',
})
export class SpoiltResPage {

  user_id: any;
  spoiltReportData: any;
  date: any;
  spoiltSum: any;

  constructor(public loadingCtrl: LoadingController,private fire: AngularFireAuth,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  this.user_id = this.fire.auth.currentUser.email;
  }

  ionViewDidLoad() {
    this.getSpoiltReport(this.date);
  }

  getSpoiltReport(date){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

     this.DB.retrieveSpoiltReport(date)
     .then((dataSpoilt)=>
     {
       let existingData = Object.keys(dataSpoilt).length;
       let sum = 0;
 
       if(existingData !== 0)
       {
               this.spoiltReportData   = [];
               for(let i = 0; i < (dataSpoilt as any).length; i ++ )
                {    
                    this.spoiltReportData.push(dataSpoilt[i].value);
                    sum+= +dataSpoilt[i].value.weight;
                }
                this.spoiltSum = sum;
       }
       else
       {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
       }
     }).then(() => {
      loader.dismiss();
     });
  }

  deleteEntry(doc)
  {
    this.DB.deleteSpoiltEntryDoc(doc).then((info) => {

      let alert = this.alertCtrl.create({
        title: 'Delete!',
        subTitle: 'Entry has been deleted',
        buttons: [{
          text: 'OK',
          role: 'cancel',
          handler: () => {
            alert.dismiss().then(()=>{
              this.navCtrl.pop();
            });
            
        }
      }]
      });
      alert.present();
    });
  }

}
