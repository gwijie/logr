import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../model/user';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  user = {} as User;
  requiredErrMsg: string;
  emailErrMsg: string;
  minErrMsg: string;
  year: any;
  month: any;
  err: any;
  loading: Loading;
  //UserAuth: any;

  constructor(public fire: AngularFireAuth,public menuCtrl: MenuController, public formBuilder: FormBuilder,public navCtrl: NavController,public authData: AuthProvider, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
    this.initializeErrors();
    
        this.loginForm = formBuilder.group({
          username: ['', Validators.compose([Validators.maxLength(30), Validators.email ,Validators.required])],
          password: ['', Validators.compose([Validators.minLength(5), Validators.pattern('^[a-zA-Z0-9-_]+$'), Validators.required])],
      });


  }

  ionViewWillEnter()
  {
    this.menuCtrl.enable(false);
  }

  openMenu() {
    this.menuCtrl.open();
  }
 
  closeMenu() {
    this.menuCtrl.close();
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  initializeErrors(){
    this.requiredErrMsg = 'This is required';
    this.minErrMsg = 'Password must be at least 5 characters long';
    this.emailErrMsg = 'Please enter a valid email address';
    this.year = new Date().getFullYear();
    this.month = new Date().getUTCMonth()+1;
  }
/*
  async loginUser(user: User){

    try{
      await this.fire.auth.signInWithEmailAndPassword(user.username,user.password)
      .then(() => {
        this.navCtrl.setRoot(HomePage);
      });
      /*
      let userauth: User = { username: user.username, password: user.password,  };   
      this.localStorage.setItem('userauth', userauth).subscribe(() => {
        console.log(userauth.username);
      }, () => {
        console.log('Error');
      });
      */
      /*
    }
    catch(e){
      this.err = e;
    }    
  }
*/

loginUser(user: User){
  if (!this.loginForm.valid){
    console.log(this.loginForm.value);
  } else {
    this.authData.loginUser(user)
    .then( authData => {
      this.navCtrl.setRoot(HomePage);
    }, error => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: [
            {
              text: "Ok",
              role: 'cancel'
            }
          ]
        });
        alert.present();
      });
    });

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });
    this.loading.present();
  }
}

  logOut(){
    this.fire.auth.signOut();
  }

}
