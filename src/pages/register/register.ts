import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../model/user';
import { LoginPage } from '../login/login';

//@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup;
  user = {} as User;
  requiredErrMsg: string;
  emailErrMsg: string;
  minErrMsg: string;

  constructor(private alertCtrl: AlertController,private fire: AngularFireAuth,public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams) {
    
    this.initializeErrors();

      this.registerForm = formBuilder.group({
        username: ['', Validators.compose([Validators.maxLength(30), Validators.email ,Validators.required])],
        password: ['', Validators.compose([Validators.minLength(5), Validators.pattern('^[a-zA-Z0-9-_]+$'), Validators.required])],
    });
  }

  initializeErrors(){
    this.requiredErrMsg = 'This is required';
    this.minErrMsg = 'Password must be at least 5 characters long';
    this.emailErrMsg = 'Please enter a valid email address';
  }

  login(){
    this.navCtrl.push(LoginPage);
  }

  async registerUser(user: User){
    try{
      const result = await this.fire.auth.createUserWithEmailAndPassword(user.username,user.password);
      
      if(result){
        let alert = this.alertCtrl.create({
          title: 'Registration Done',
          subTitle: 'User has successfully been registered',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
      }

    }
    catch(e){
      console.log(e);
    }
  }
}
