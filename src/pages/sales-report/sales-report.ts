import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { SalesResPage } from '../sales-res/sales-res';

//@IonicPage()
@Component({
  selector: 'page-sales-report',
  templateUrl: 'sales-report.html',
})
export class SalesReportPage {
  date: string;
  salesReportData: any;

  constructor(public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad SalesReportPage');
  }

  onSelectChange(selectedValue: any)
  {
    this.date = selectedValue.year+'-'+selectedValue.month+'-'+selectedValue.day;
    //console.log('Selected', this.date);
  }

  getReport(){
    let data = {
      date: this.date,
    }

   this.navCtrl.push(SalesResPage,data);
  }
}
