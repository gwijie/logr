import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

//@IonicPage()
@Component({
  selector: 'page-statement-res',
  templateUrl: 'statement-res.html',
})
export class StatementResPage {

  statementData: any;
  dat: any;
  no: any;
  combo: any;
  totalSum: any;

  constructor(public loadingCtrl: LoadingController,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidEnter() {
    this.dat = this.navParams.get('date');
    this.no = this.navParams.get('no');
    this.combo = this.dat+'-'+this.no;

    this.getStatement(this.combo);
  }


  getStatement(m){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.DB.retrieveStatement(m)
    .then((dataStatement)=>
    {
      let existingData = Object.keys(dataStatement).length;

      if(existingData !== 0)
      {
              this.statementData   = [];
              let sum = 0;
              for(let i = 0; i < (dataStatement as any).length; i ++ )
               {    
                   this.statementData.push(dataStatement[i].value.document); 
                   sum += +dataStatement[i].value.document.weight;
                }

               this.totalSum = sum;  
      }
      else
      {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'OK',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
      }

    }).then(() => {
      loader.dismiss();
   });
   
  }

  deleteSpoiltEntry(doc)
  {
    this.DB.deleteEntryDoc(doc).then((info) => {
      let alert = this.alertCtrl.create({
        title: 'Delete!',
        subTitle: 'Entry has been deleted',
        buttons: [{
          text: 'OK',
          role: 'cancel',
          handler: () => {
          this.navCtrl.pop();
        }
      }]
      });

      alert.present();
    });
  }

}
