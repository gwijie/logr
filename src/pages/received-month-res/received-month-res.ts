import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular';

//@IonicPage()
@Component({
  selector: 'page-received-month-res',
  templateUrl: 'received-month-res.html',
})
export class ReceivedMonthResPage {

  receivedMonthReportData: any;
  dat: any;
  monthReceivedSum: any;
  user_id: any;

  constructor(public loadingCtrl: LoadingController,private fire: AngularFireAuth,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
  }

  ionViewDidLoad() {
    this.dat = this.navParams.get('date');
    this.getReceivedMonthReport(this.dat);
  }

  getReceivedMonthReport(datei){
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

     this.DB.retrieveReceivedMonthReport(datei)
     .then((dataSales)=>
     {
       let existingData = Object.keys(dataSales).length;
       let sum = 0;
       if(existingData !== 0)
       {
               this.receivedMonthReportData   = [];
 
               for(let i = 0; i < (dataSales as any).length; i ++ )
                {    
                    this.receivedMonthReportData.push(dataSales[i].value.document);  
                    sum += +dataSales[i].value.document.weight;
                }
                
                this.monthReceivedSum = sum;
       }
       else
       {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
       }
     }).then(() => {
       loader.dismiss();
     });
  }

  deleteEntry(doc)
  {

    this.DB.deleteReceivedEntryDoc(doc).then((info) => {

      let alert = this.alertCtrl.create({
        title: 'Delete!',
        subTitle: 'Entry has been deleted',
        buttons: [{
          text: 'OK',
          role: 'cancel',
          handler: () => {
            
            alert.dismiss();
        }
      }]
      });

      alert.present();
    });
  }

}
