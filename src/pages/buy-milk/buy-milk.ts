import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';

//@IonicPage()
@Component({
  selector: 'page-buy-milk',
  templateUrl: 'buy-milk.html',
})
export class BuyMilkPage {

  buyMilkForm: FormGroup;
  requiredErrMsg: string;
  volumePatternErrMsg: string;
  maxErrMsg: string;

  member: any;
  no: any;
  name: any;
  station: any;
  stations: any;
  user_id: any;
  weight: any;
  receiveStation: any;

  constructor(private fire: AngularFireAuth,private alertCtrl: AlertController,public formBuilder: FormBuilder,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
    this.initializedReceiveErrors();
    
        this.buyMilkForm = formBuilder.group({
          weight: ['', Validators.compose([Validators.maxLength(7),Validators.required, Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')])],
          receiveStation: ['', Validators.compose([Validators.required])],
      });
  }

  ionViewDidLoad() {
    
    this.no = this.navParams.get('no');
    this.name = this.navParams.get('name');
    this.station = this.navParams.get('station');

    this.loadStations();
  }

  loadStations()
  {
    this.DB.fetchStations().then((result) =>
    {
      this.stations = [];
      
          let row = (result as any).rows;   
          for(let i = 0; i < row.length; i ++ )
          {    
              this.stations.push(row[i].doc);   
          }

    });

  }

  initializedReceiveErrors(){
    this.requiredErrMsg = 'This is required';
    this.volumePatternErrMsg = 'Please enter a valid weight eg. 5.80';
    this.maxErrMsg = 'Field must have not more than 6 digits'; 
  }

  saveEntry()
  {
    //console.log(this.buyMilkForm.controls['weight'].value);
    //console.log(no+''+name+''+station)
    
    var ddate = new Date();
    var tiime = ddate.toTimeString().substr(0,8);
    var daate = ddate.toISOString().substr(0,10);
    var created_at = daate+' '+tiime;
    let receivedObject = this.buyMilkForm.controls['receiveStation'].value;

    var entry = {
      _id: new Date().toJSON() + Math.random(),
      weight: this.buyMilkForm.controls['weight'].value,
      name: this.name,
      member_no: this.no,
      station: this.station,
      date: new Date().toString().substr(0,25),
      procurement_date: created_at,
      type: 'Normal',
      created_at: created_at,
      user_id: this.user_id,
      station_id: receivedObject.route_id,
      station_name: receivedObject.route_name,
      db: 'procure',
    }

    this.DB.createEntry(entry).then((result) =>
  {
    if(result)
    {
        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Entry has been recorded.',
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
              handler: () => {
                this.buyMilkForm.reset();
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();
    }
    else
    {
      let alert = this.alertCtrl.create({
        title: 'Oops',
        subTitle: 'Seem a error has occured',
        buttons: [
          {
            text: 'Ok',
            role: 'cancel',
            handler: () => {
              this.buyMilkForm.reset();
            }
          }
        ]
      });
      alert.present();
    }
  });

  }

}
