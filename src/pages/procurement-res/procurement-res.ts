import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';


//@IonicPage()
@Component({
  selector: 'page-procurement-res',
  templateUrl: 'procurement-res.html',
})
export class ProcurementResPage {
  procurementReportData: any;
  dat: any;
  procureSum: any;
  user_id: any;

  constructor(public loadingCtrl: LoadingController,private fire: AngularFireAuth,private alertCtrl: AlertController,public DB: DatabaseProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
  }


  ionViewDidLoad() {
    this.dat = this.navParams.get('date');
    this.getProcurementReport(this.dat);
  }

  getProcurementReport(datei){

    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

     this.DB.retrieveProcurementReport(datei)
     .then((dataSales)=>
     {
       let existingData = Object.keys(dataSales).length;
 
       if(existingData !== 0)
       {
               this.procurementReportData   = [];
               let sum = 0;
               for(let i = 0; i < (dataSales as any).length; i ++ )
                {    
                    this.procurementReportData.push(dataSales[i].value); 
                    sum += +dataSales[i].value.weight;
                }
                this.procureSum = sum;       
       }
       else
       {
        let alert = this.alertCtrl.create({
          title: 'We got nothing',
          subTitle: 'Data is not available',
          buttons: [{
            text: 'Dismiss',
            role: 'cancel',
            handler: () => {
            this.navCtrl.pop();
          }
        }]
        });
        alert.present();
       }
     }).then(() => {
        loader.dismiss();
     });
  }

}
