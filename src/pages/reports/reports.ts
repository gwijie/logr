import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SalesReportPage } from '../sales-report/sales-report';
import { ReceivedReportsPage } from '../received-reports/received-reports';
import { ReceivedMonthReportsPage } from '../received-month-reports/received-month-reports';
import { ProcurementReportPage } from '../procurement-report/procurement-report';
import { SalesMonthReportPage } from '../sales-month-report/sales-month-report';
import { StatementReportPage } from '../statement-report/statement-report';
import { SpoiltReportPage } from '../spoilt-report/spoilt-report';
import { AngularFireAuth } from 'angularfire2/auth';


//@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  user_id: any;

  constructor(private fire: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
    this.user_id = this.fire.auth.currentUser.email;
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ReportsPage');
  }

  salesReport(){
    this.navCtrl.push(SalesReportPage);
  }

  receiveReport(){
    this.navCtrl.push(ReceivedReportsPage);
  }

  procureReport(){
    this.navCtrl.push(ProcurementReportPage);
  }

  monthSaleReport(){
    this.navCtrl.push(SalesMonthReportPage);
  }

  monthReceivedReport(){
    this.navCtrl.push(ReceivedMonthReportsPage);
  }

  retrieveStatement(){
    this.navCtrl.push(StatementReportPage);
  }

  spoiltReport(){
    this.navCtrl.push(SpoiltReportPage);
  }


}
