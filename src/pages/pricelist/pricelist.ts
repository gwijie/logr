import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { Product } from '../../model/product';
import { FormBuilder } from '@angular/forms';

//@IonicPage()
@Component({
  selector: 'page-pricelist',
  templateUrl: 'pricelist.html',
})
export class PricelistPage {

  product = {} as Product;
  productName: any;
  requiredErrMsg: string;
  maxErrMsg: string;
  private lists;

  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public DB: DatabaseProvider) {

  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad PricelistPage');
    this.loadPricelist();
  }


  loadPricelist(){

    this.DB.fetchProducts().then((result) =>
  {
    this.lists = [];
    
        let row = (result as any).rows;
              
        for(let i = 0; i < row.length; i ++ )
        {    
            this.lists.push(row[i].doc);   
        }
  });
  }

}
