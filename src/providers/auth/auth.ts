import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../model/user';

@Injectable()
export class AuthProvider {

  constructor(public fire: AngularFireAuth) {
    
  }

  loginUser(user: User): Promise<any> {
    return this.fire.auth.signInWithEmailAndPassword(user.username,user.password);
  }

  logoutUser(): Promise<void> {
    return this.fire.auth.signOut();
  }

  signupUser(user: User): Promise<any> {
    return this.fire.auth.createUserWithEmailAndPassword(user.username,user.password);
  }

}
