import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
//import PouchdbFind from 'pouchdb-find';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable()
export class DatabaseProvider {
  private logrDB: any;
  private remlogrDB: any;

  spoilt: any;
  procure: any;
  procureMonth: any;
  procureSum: any;
  received: any;
  receivedMonth: any;
  sales: any;
  salesMonth: any;
  feeds: any;
  memberDits: any;
  limit: any;
  limitDate: any;
  year: any;
  month: any;
  day: any;
  da: any;
  det: any;
  salesReport: any;
  salesMonthReport: any;
  receivedReport: any;
  receivedMonthReport: any;
  statementReport: any;
  spoiltReport: any;
  user_id: any;

  constructor(private fire: AngularFireAuth,public http: Http,public alertCtrl : AlertController) {
    this.initialiseDB();
    this.syncEverything();
    this.user_id = this.fire.auth.currentUser.email;
    //PouchDB.debug.enable('pouchdb:api');
  }

  initialiseDB()
  {
    this.remlogrDB = new PouchDB('http://139.59.68.144:5984/logr');
    this.logrDB = new PouchDB('logr',{ adapter : 'websql',  auto_compaction: true});
  }

  pullDB(){
            let options = {
              retry: true,
              since: 'now',
            }; 
  
            return this.logrDB.replicate.from(this.remlogrDB, options)
            .on('change', function (info) {
              console.log('changing...');
            }).on('paused', function (err) {
              console.log('paused');
            }).on('active', function () {
              console.log('active');
            }).on('denied', function (err) {
              console.log('denied');
            }).on('complete', function (info) {
              console.log('complete');
            }).on('error', function (err) {
              alert(err);
            });
  }

   syncEverything()
  {

          let options = {
            live: true,
            retry: true,
            continuous: true
          }; 

          this.logrDB.sync(this.remlogrDB, options)
          .on('change', function (info) {
            console.log('sync changing...');
          }).on('paused', function (err) {
            console.log('paused');
          }).on('active', function () {
            console.log('sync active');
          }).on('denied', function (err) {
            console.log('denied');
          }).on('complete', function (info) {
            console.log('sync complete');
          }).on('error', function (err) {
            alert(err);
          });
      
        
  }

  change(){
  return this.logrDB.changes({
    live: true, 
    since: 'now', 
    include_docs: true
  });
}

    async retrieveProcure()
  {
    try {
    await new Date();
    this.limitDate = new Date().toJSON().substring(0,10);
    }
    finally{
    return new Promise(resolve => {
      if(this.user_id === 'stephen@conradgray.com' || this.user_id === 'admin@kirimiridairy.co.ke')
      {
      this.logrDB.query('daily_dits/daily_dits',{
        include_docs: false,
        key: this.limitDate,
      })
      .then((result) => {
        this.procure = [];
        result.rows.map((row) => {
          this.procure.push(row);
        });
        resolve(this.procure)
      }).catch((error) => {
        console.log(error);
      });
    }
    else
    {
      this.logrDB.query('dairy_date_weight/dairy_date_weight',{
        include_docs: false,
        key: this.limitDate+'-'+this.user_id,
      })
      .then((result) => {
        this.procure = [];
        result.rows.map((row) => {
          this.procure.push(row);
        });
        resolve(this.procure)
      }).catch((error) => {
        console.log(error);
      });
    }
    });
  }
  }

  async retrieveMonthlyProcure()
  {
    try {
    await new Date();
    this.limitDate = '';
    this.year = new Date().getFullYear();
    this.month = new Date().toJSON().substring(5,7);
    //this.day = new Date().getDate();
    this.da = new Date(this.year,this.month,this.day).toJSON().substr(8,2);
    this.limitDate = this.year+'-'+this.month;
    }
    finally{
    return new Promise(resolve => {
      this.logrDB.query('month_dits/month_dits',{
        include_docs: false,
        key: this.limitDate
      })
      .then((result) => {
        this.procureMonth = [];
        result.rows.map((row) => {
          this.procureMonth.push(row);
        });
        resolve(this.procureMonth)
      }).catch((error) => {
        console.log(error);
      });

    });
  }
  }

  retrieveMonthlySpoilt()
  {
    return new Promise(resolve => {
      this.logrDB.query('month_spoilt/month_spoilt',{
        include_docs: false,
        key: this.year+'-'+this.month
      })
      .then((result) => {
        this.spoilt = [];
        result.rows.map((row) => {
          this.spoilt.push(row);
        });
        resolve(this.spoilt)
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  async retrieveSales()
  {
    try {
      await new Date();
      this.limitDate = new Date().toJSON().substring(0,10);
      }
      finally{
      return new Promise(resolve => {
        if(this.user_id === 'stephen@conradgray.com' || this.user_id === 'admin@kirimiridairy.co.ke')
        {
        this.logrDB.query('date_sales/date_sales',{
          include_docs: true,
          key: this.limitDate
        }).then((result) => {
          this.sales = []
          result.rows.map((row) => {
            this.sales.push(row);
            
          });
          resolve(this.sales);
        }).catch((error) => {
          console.log(error);
        });
      }
      else
      {
        this.logrDB.query('date_sales_user/date_sales_user',{
          include_docs: true,
          key: this.limitDate+'-'+this.user_id
        }).then((result) => {
          this.sales = []
          result.rows.map((row) => {
            this.sales.push(row);
            
          });
          resolve(this.sales);
        }).catch((error) => {
          console.log(error);
        });
      }
      });
  }
  }

  retrieveSalesReport(tarehe)
  {
    return new Promise(resolve => {
      this.logrDB.query('date_sales/date_sales',{
        include_docs: true,
        key: tarehe,
      }).then((result) => {
        this.salesReport = []
        result.rows.map((row) => {
          this.salesReport.push(row);
        });
        resolve(this.salesReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  retrieveSalesMonthReport(tarehe)
  {
    return new Promise(resolve => {
      this.logrDB.query('month_sales/month_sales',{
        include_docs: true,
        key: tarehe,
      }).then((result) => {
        this.salesMonthReport = []
        result.rows.map((row) => {
          this.salesMonthReport.push(row);
        });
        resolve(this.salesMonthReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  retrieveReceivedReport(tarehe)
  {
    return new Promise(resolve => {
      this.logrDB.query('date_received/date_received',{
        include_docs: true,
        key: tarehe,
      }).then((result) => {
        this.receivedReport = []
        result.rows.map((row) => {
          this.receivedReport.push(row);
        });
        resolve(this.receivedReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  retrieveSpoiltReport(tarehe)
  {
    return new Promise(resolve => {
      this.logrDB.query('month_spoilt/month_spoilt',{
        include_docs: true,
        key: tarehe,
      }).then((result) => {
        this.spoiltReport = []
        result.rows.map((row) => {
          this.spoiltReport.push(row);
        });
        resolve(this.spoiltReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  retrieveReceivedMonthReport(tarehe)
  {
    return new Promise(resolve => {
      this.logrDB.query('month_received/month_received',{
        include_docs: true,
        key: tarehe,
      }).then((result) => {
        this.receivedMonthReport = []
        result.rows.map((row) => {
          this.receivedMonthReport.push(row);
        });
        resolve(this.receivedMonthReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  retrieveProcurementReport(tarehe)
  {
    if(this.user_id === 'stephen@conradgray.com' || this.user_id === 'dianarigu@gmail.com' || this.user_id === 'admin@kirimiridairy.co.ke')
    {
      return new Promise(resolve => {
        this.logrDB.query('daily_dits/daily_dits',{
          include_docs: true,
          key: tarehe,
        }).then((result) => {
          this.receivedReport = []
          result.rows.map((row) => {
            this.receivedReport.push(row);
          });
          resolve(this.receivedReport);
        }).catch((error) => {
          console.log(error);
        });
      });
   }
   else
   {
    return new Promise(resolve => {
      this.logrDB.query('dairy_date_weight/dairy_date_weight',{
        include_docs: true,
        key: tarehe+'-'+this.user_id,
      }).then((result) => {
        this.receivedReport = []
        result.rows.map((row) => {
          this.receivedReport.push(row);
        });
        resolve(this.receivedReport);
      }).catch((error) => {
        console.log(error);
      });
    });
   }
  }

  retrieveStatement(tarehe){
    return new Promise(resolve => {
      this.logrDB.query('date_number_weight/date_number_weight',{
        include_docs: true,
        timeout: 1000000,
        key: tarehe,
      }).then((result) => {
        this.statementReport = []
        result.rows.map((row) => {
          this.statementReport.push(row);
        });
        resolve(this.statementReport);
      }).catch((error) => {
        console.log(error);
      });
    });
  }
  
  async retrieveMonthlySales()
  {
    try {
      await new Date();
      this.year = new Date().getFullYear();
      this.month = new Date().toJSON().substring(5,7);
      //this.day = new Date().getDate()+1;
  
      this.da = new Date(this.year,this.month,this.day).toJSON().substr(8,2);
      this.limitDate = this.year+'-'+this.month;
  
      //console.log(this.limitDate);
      }
      finally{

    return new Promise(resolve => {
   
      this.logrDB.query('month_sales/month_sales',{
        include_docs: true,
        key: this.limitDate
      }).then((result) => {
        this.salesMonth = [];
        result.rows.map((row) => {
          this.salesMonth.push(row);
        });
        resolve(this.salesMonth);
      }).catch((error) => {
        console.log(error);
      });
    });
  }
  }

  async retrieveReceived()
  {
    try {
    await new Date();
    this.limitDate = '';
    this.year = new Date().getFullYear();
    this.month = new Date().toJSON().substring(5,7);
    this.day = new Date().getDate()+1;

    this.da = new Date(this.year,this.month,this.day).toJSON().substr(8,2);
    this.limitDate = this.year+'-'+this.month+'-'+this.da;

    //console.log(this.limitDate+' received today');
    }
    finally{
   
    return new Promise(resolve => {
      this.logrDB.query('date_received/date_received',{
        include_docs: false,
        key: this.limitDate
      })
      .then((result) => {
        this.received = [];
        result.rows.map((row) => {
          this.received.push(row);
        });
        resolve(this.received)
      }).catch((error) => {
        console.log(error);
      });
    });
  }
  }

  async retrieveMonthlyReceived()
  {
    try {
      await new Date();
      this.limitDate = '';
      this.year = new Date().getFullYear();
      this.month = new Date().toJSON().substring(5,7);
      //this.day = new Date().getDate()+1;
      //this.da = new Date(this.year,this.month,this.day).toJSON().substr(8,2);
      this.limitDate = this.year+'-'+this.month;
      }
      finally{
      return new Promise(resolve => {
        this.logrDB.query('month_received/month_received',{
          include_docs: false,
          key: this.limitDate
        })
        .then((result) => {
            this.receivedMonth = [];
            result.rows.map((row) => {
            this.receivedMonth.push(row);
          });
          resolve(this.receivedMonth);
        }).catch((error) => {
          console.log(error);
        });
      });
    }
  }

  fetchProducts()
  {    
  return new Promise(resolve => 
  {
    this.logrDB.query('products/products',{
      include_docs: true  
    })
    .then(
      (result) => {
        resolve(result);
        });
    });
  }

  fetchCustomers()
  {
    return new Promise(resolve => 
      {
        this.logrDB.query('customers/customers',{
          include_docs: true
        })
        .then(
          (result) => {
            resolve(result);
            });
        });
  }

  fetchStations(){
    return new Promise(resolve => 
      {
        this.logrDB.query('routes/routes',{
          include_docs: true
        })
        .then(
          (result) => {
            resolve(result);
            });
        });
  }

  createSale(data){
    return new Promise(resolve =>
      {
         this.logrDB.put(data);
         resolve(true);
     });
  }

  createEntry(data){
    return new Promise(resolve =>
      {
         this.logrDB.put(data);
         resolve(true);
     });
  }

  createReceived(data){
    return new Promise(resolve =>
      {
         this.logrDB.put(data);
         resolve(true);
     });
  }

  createSpoilt(data){
    return new Promise(resolve =>
      {
         this.logrDB.put(data);
         resolve(true);
     });
  }

  deleteEntryDoc(doc){
    return new Promise(resolve =>
      {
         this.logrDB.remove(doc);
         resolve(true);
     });
  }

  deleteProcEntryDoc(doc){
    return new Promise(resolve =>
      {
         this.logrDB.remove(doc);
         resolve(true);
     });
  }

  deleteSalesEntryDoc(doc){
    return new Promise(resolve =>
      {
         this.logrDB.remove(doc);
         resolve(true);
     });
  }

  deleteReceivedEntryDoc(doc){
    return new Promise(resolve =>
      {
         this.logrDB.remove(doc);
         resolve(true);
     });
  }

  deleteSpoiltEntryDoc(doc){
    return new Promise(resolve =>
      {
         this.logrDB.remove(doc);
         resolve(true);
     });
  }

  lookUp(membershipNo)
  {
    return new Promise(resolve => {
      this.logrDB.query('members/members',{
        include_docs: true,
        key: membershipNo
      }).then(
        (result) => {
          resolve(result);
          });
    });
  }

}
