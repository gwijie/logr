import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';


@Injectable()
export class DatabaseProvider {

  private spoiltDB    : any;
  private procureDB   : any;
  private receivedDB  : any;
  private salesDB     : any;
  private feedsDB     : any;
  private membersDB   : any;
  private customersDB : any;
  private productsDB  : any;
  private stationsDB  : any;

  private remspoiltDB    : any;
  private remprocureDB   : any;
  private remreceivedDB  : any;
  private remsalesDB     : any;
  private remfeedsDB     : any;

  spoilt: any;
  procure: any;
  procureSum: any;
  received: any;
  sales: any;
  feeds: any;
  memberDits: any;
  
  year: any;
  month: any;

  constructor(public http: Http,public alertCtrl : AlertController) {
    this.initialiseDB();
    PouchDB.plugin(PouchdbFind);
  }

  initialiseDB()
  {
     this.spoiltDB = new PouchDB('spoilt',{adapter : 'websql'});
     this.procureDB = new PouchDB('procure',{adapter : 'websql'});
     this.receivedDB = new PouchDB('received',{adapter : 'websql'});
     this.salesDB = new PouchDB('sales',{adapter : 'websql'});
     this.feedsDB = new PouchDB('feeds',{adapter : 'websql'});
     this.membersDB = new PouchDB('wakulima',{adapter : 'websql'});
     this.customersDB = new PouchDB('customers',{adapter : 'websql'});
     this.productsDB = new PouchDB('products',{adapter : 'websql'});
     this.stationsDB = new PouchDB('stations',{adapter : 'websql'});

     this.remspoiltDB = new PouchDB('http://139.59.68.144:5984/spoilt');
     this.remprocureDB = new PouchDB('http://139.59.68.144:5984/procure');
     this.remreceivedDB = new PouchDB('http://139.59.68.144:5984/received');
     this.remsalesDB = new PouchDB('http://139.59.68.144:5984/sales');
     this.remfeedsDB = new PouchDB('http://139.59.68.144:5984/feeds');
     
     this.year = new Date().getFullYear();
     this.month = new Date().getUTCMonth()+1;
  }

  retrieveSpoilt()
  {
    if (this.spoilt) {
      return Promise.resolve(this.spoilt);
    }
   
    return new Promise(resolve => {
   
      this.spoiltDB.allDocs({
   
        include_docs: true
   
      }).then((result) => {
   
        this.spoilt = [];
   
        result.rows.map((row) => {
          this.spoilt.push(row);
        });
   
        resolve(this.spoilt);
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });
  }

  syncingMembers(memberz)
    {
       return new Promise(resolve =>
       {
          this.membersDB.put(memberz).catch((err) =>
          {
             console.log(err)
          });

          console.log(memberz);
 
          resolve(true);
 
      });
    }

    syncingCustomers(customerz)
    {
       return new Promise(resolve =>
       {
          this.customersDB.put(customerz).catch((err) =>
          {
             console.log(err)
          });

          console.log(customerz);
 
          resolve(true);
 
      });
    }

    syncingPricelist(productz)
    {
       return new Promise(resolve =>
       {
          this.productsDB.put(productz).catch((err) =>
          {
             console.log(err)
          });

          console.log(productz);
 
          resolve(true);
 
      });
    }

    syncingStation(stationz)
    {
       return new Promise(resolve =>
       {
          this.stationsDB.put(stationz).catch((err) =>
          {
             console.log(err)
          });

          console.log(stationz);
 
          resolve(true);
 
      });
    }

  retrieveProcure()
  {
    if (this.procure) {
      return Promise.resolve(this.procure);
    }
   
    return new Promise(resolve => {
   
      this.procureDB.query('daily_dits/daily_dits',{
        include_docs: false,
        key: this.year+'-'+this.month
      })
      .then((result) => {

        this.procure = [];

        result.rows.map((row) => {
          this.procure.push(row);
          
        });
   
        resolve(this.procure);
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });
  }

  retrieveSales()
  {
    if (this.sales) {
      return Promise.resolve(this.sales);
    }
   
    return new Promise(resolve => {
   
      this.salesDB.allDocs({
   
        include_docs: true
   
      }).then((result) => {
   
        this.sales = [];
   
        result.rows.map((row) => {
          this.sales.push(row);
        });
   
        resolve(this.sales);
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });
  }

  retrievefeeds()
  {
    if (this.feeds) {
      return Promise.resolve(this.feeds);
    }
   
    return new Promise(resolve => {
   
      this.feedsDB.allDocs({
   
        include_docs: true
   
      }).then((result) => {
   
        this.feeds = [];
   
        result.rows.map((row) => {
          this.feeds.push(row);
        });
   
        resolve(this.feeds);
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });
  }

  retrieveReceived()
  {
    if (this.received) {
      return Promise.resolve(this.received);
    }
   
    return new Promise(resolve => {
   
      this.receivedDB.allDocs({
   
        include_docs: true
   
      }).then((result) => {
   
        this.received = [];
   
        result.rows.map((row) => {
          this.received.push(row);
        });
   
        resolve(this.received);
   
      }).catch((error) => {
   
        console.log(error);
   
      });
   
    });
  }

  lookUp(membershipNo)
  { 
    return new Promise(resolve => {
      this.membersDB.createIndex({
        index: {fields: ['membershipNo']}
      })
      .then(() => {
        this.membersDB.find({
          fields: ['membershipNo','fullNames','station'],
          selector: {
            membershipNo: {$eq: membershipNo}
          }
        })
        .then((result) => {
          
          this.memberDits = result.docs;
          resolve(this.memberDits);

          console.log(result);
          
        });
      });
    });
  }

}
