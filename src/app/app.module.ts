import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp } from 'ionic-angular';
import { IonicErrorHandler } from 'ionic-angular'; 
import { IonicModule } from 'ionic-angular';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatabaseProvider } from '../providers/database/database';
import { HomePage } from '../pages/home/home';
import { LookupPage } from '../pages/lookup/lookup';
import { PricelistPage } from '../pages/pricelist/pricelist';
import { SalePage } from '../pages/sale/sale';
import { ReceivedPage } from '../pages/received/received';
import { SpoiltPage } from '../pages/spoilt/spoilt';
import { ReportsPage } from '../pages/reports/reports';
import { SettingsPage } from '../pages/settings/settings';
import { AboutPage } from '../pages/about/about';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { BuyMilkPage } from '../pages/buy-milk/buy-milk';
import { IssueFeedPage } from '../pages/issue-feed/issue-feed';
//import { AsyncLocalStorageModule } from 'angular-async-local-storage';
import { SalesReportPage } from '../pages/sales-report/sales-report';
import { ReceivedReportsPage } from '../pages/received-reports/received-reports';
import { ReceivedMonthReportsPage } from '../pages/received-month-reports/received-month-reports';
import { ProcurementReportPage } from '../pages/procurement-report/procurement-report';
import { SalesResPage } from '../pages/sales-res/sales-res';
import { ReceivedResPage } from '../pages/received-res/received-res';
import { ReceivedMonthResPage } from '../pages/received-month-res/received-month-res';
import { ProcurementResPage } from '../pages/procurement-res/procurement-res';
import { SalesMonthReportPage } from '../pages/sales-month-report/sales-month-report';
import { SalesMonthResPage } from '../pages/sales-month-res/sales-month-res';
import { StatementReportPage } from '../pages/statement-report/statement-report';
import { StatementResPage } from '../pages/statement-res/statement-res';
import { SpoiltResPage } from '../pages/spoilt-res/spoilt-res';
import { SpoiltReportPage } from '../pages/spoilt-report/spoilt-report';
import { AuthProvider } from '../providers/auth/auth';


const firebaseAuth = {
  apiKey: "AIzaSyAVVsG1UxDunq4gylGCuODD9igaCumccMk",
  authDomain: "quotes-705c1.firebaseapp.com",
  databaseURL: "https://quotes-705c1.firebaseio.com",
  projectId: "quotes-705c1",
  storageBucket: "quotes-705c1.appspot.com",
  messagingSenderId: "695997859708"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LookupPage,
    PricelistPage,
    SalePage,
    ReceivedPage,
    SpoiltPage,
    ReportsPage,
    SettingsPage,
    AboutPage,
    RegisterPage,
    LoginPage,
    IssueFeedPage,
    BuyMilkPage,
    SalesReportPage,
    SalesResPage,
    ReceivedReportsPage,
    ReceivedResPage,
    ProcurementReportPage,
    ProcurementResPage,
    SalesMonthReportPage,
    SalesMonthResPage,
    ReceivedMonthReportsPage,
    ReceivedMonthResPage,
    StatementReportPage,
    StatementResPage,
    SpoiltResPage,
    SpoiltReportPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    Ng2SearchPipeModule,
    //AsyncLocalStorageModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LookupPage,
    PricelistPage,
    SalePage,
    ReceivedPage,
    SpoiltPage,
    ReportsPage,
    SettingsPage,
    AboutPage,
    RegisterPage,
    LoginPage,
    BuyMilkPage,
    IssueFeedPage,
    SalesReportPage,
    SalesResPage,
    ReceivedReportsPage,
    ReceivedResPage,
    ProcurementReportPage,
    ProcurementResPage,
    SalesMonthReportPage,
    SalesMonthResPage,
    ReceivedMonthReportsPage,
    ReceivedMonthResPage,
    StatementReportPage,
    StatementResPage,
    SpoiltResPage,
    SpoiltReportPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    AuthProvider
  ]
})
export class AppModule {}
