import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { LookupPage } from '../pages/lookup/lookup';
import { PricelistPage } from '../pages/pricelist/pricelist';
import { SalePage } from '../pages/sale/sale';
import { ReceivedPage } from '../pages/received/received';
import { SpoiltPage } from '../pages/spoilt/spoilt';
import { ReportsPage } from '../pages/reports/reports';
import { SettingsPage } from '../pages/settings/settings';
import { AuthProvider } from '../providers/auth/auth';
import { AngularFireAuth } from 'angularfire2/auth';
//import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
//import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  emailAd: any;
  loading: any;

  options: any = 'options';
  search: any = 'search';
  clipboard: any = 'clipboard';
  personAdd: any = 'person-add';
  unlock: any = 'unlock';
  pricetag: any = 'pricetag';
  cash: any = 'cash';
  beaker: any = 'beaker';
  alert: any = 'alert';
  information: any = 'information-circle';
  settings: any = 'settings';
  pie: any = 'pie';

  danger: any = 'danger';
  dark: any = 'dark';
  royal: any = 'royal';
  warning: any = 'warning';
  secondary: any = 'secondary';
  primary: any = 'primary';


  pages: Array<{title: string, component: any, icon: any, color: any }>;
  pagez: Array<{title: string, component: any, icon: any, color: any }>;

  constructor(public platform: Platform,public authData: AuthProvider, fire: AngularFireAuth, public statusBar: StatusBar, public splashScreen: SplashScreen, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.initializeApp();
    
    this.pages = [
      { title: 'Dashboard', component: HomePage, icon: this.options, color: this.danger },
      { title: 'Look Up', component: LookupPage, icon: this.search, color: this.dark },
      { title: 'Pricelist', component: PricelistPage, icon: this.pricetag, color: this.warning },
      { title: 'Sales', component: SalePage, icon: this.cash, color: this.secondary },
      { title: 'Received', component: ReceivedPage, icon: this.beaker, color: this.primary },
      { title: 'Spoilt', component: SpoiltPage, icon: this.alert, color: this.danger },
      { title: 'Reports', component: ReportsPage, icon: this.pie, color: this.primary },
      { title: 'Settings', component: SettingsPage, icon: this.settings, color: this.danger },
      { title: 'About', component: AboutPage, icon: this.information, color: this.dark },
      { title: 'Register', component: RegisterPage, icon: this.personAdd, color: this.warning },
    ];

    this.pagez = [
      { title: 'Dashboard', component: HomePage, icon: this.options, color: this.danger },
      { title: 'Look Up', component: LookupPage, icon: this.search, color: this.dark },
      { title: 'Pricelist', component: PricelistPage, icon: this.pricetag, color: this.warning },
      { title: 'Sales', component: SalePage, icon: this.cash, color: this.secondary },
      { title: 'Received', component: ReceivedPage, icon: this.beaker, color: this.primary },
      { title: 'Spoilt', component: SpoiltPage, icon: this.alert, color: this.danger },
      { title: 'Reports', component: ReportsPage, icon: this.pie, color: this.primary },
      { title: 'Settings', component: SettingsPage, icon: this.settings, color: this.danger },
      { title: 'About', component: AboutPage, icon: this.information, color: this.dark },
    ];

    const authObserver = fire.authState.subscribe( user => {
      if (user) {
        this.rootPage = HomePage;
        this.emailAd = fire.auth.currentUser.email;
        authObserver.unsubscribe();
      } else {
        this.rootPage = LoginPage;
        authObserver.unsubscribe();
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }

  logoutUser(){
    
      this.authData.logoutUser()
      .then( authData => {
        this.nav.setRoot(LoginPage);
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    
  }
}
